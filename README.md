# BACKEND CON PYTHON (FASTAPI-SQLALCHEMY) BASE DE DATOS SQLITE CON ARQUITECTURA POR CAPAS


## Inicia el servidor de **FastAPI** ejecutando el siguiente comando:

``uvicorn app:app --reload``

El argumento `--reload` activa el reinicio automático del servidor cada vez que se guardan cambios en el archivo app.py.

Configura el puerto en el que se ejecutará el servidor de **FastAPI** en el archivo `.env`, normalmente el puerto 8000 es el que se utiliza por defecto.

``PORT=8000``

Abre tu navegador web y visita http://localhost:{**puerto**}/docs para ver la documentación de la API generada automáticamente por **FastAPI**.