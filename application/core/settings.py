import os
from dotenv import load_dotenv

load_dotenv()


class Settings:
    ADMIN = os.getenv("ADMIN")
    SECRET_KEY = os.getenv("SECRET_KEY")
    ALGORITHM = os.getenv("ALGORITHM")
    ACCESS_TOKEN_EXPIRE_MINUTES = os.getenv("ACCESS_TOKEN_EXPIRE_MINUTES")
