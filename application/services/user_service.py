from typing import List

from ..repositories import UserRepository
from ..models import UserDTO, UserRoleDTO, UserPasswordDTO, User


class UserService:
    def __init__(self, repository: UserRepository):
        self.repository = repository

    def create_user(self, user_data: UserDTO) -> User:
        user = self.repository.create_user(user_data)
        return user

    def get_all_users(self) -> List[User]:
        users = self.repository.get_all_users()
        return users

    def get_active_users(self) -> List[User]:
        users = self.repository.get_active_users()
        return users

    def get_user_by_id(self, id: int) -> User:
        user = self.repository.get_user_by_id(id)
        return user

    def get_user_by_username(self, username: str) -> User:
        user = self.repository.get_user_by_username(username)
        return user

    def update_user_password(self, user_id: int, user_password: UserPasswordDTO, current_user: User) -> None:
        user = self.get_user_by_id(user_id)
        if not user:
            raise Exception("User not found")
        self.repository.update_user_password(user, user_password, current_user)

    def update_user_role(self, user_id: int, user_role: UserRoleDTO, current_user: User) -> None:
        user = self.get_user_by_id(user_id)
        if not user:
            raise Exception("User not found")
        self.repository.update_user_role(user, user_role, current_user)

    def activate_user(self, user_id: int, current_user: User) -> None:
        user = self.get_user_by_id(user_id)
        if not user:
            raise Exception("User not found")
        self.repository.activate_user(user, current_user)

    def remove_user(self, user_id: int, current_user: User) -> None:
        user = self.get_user_by_id(user_id)
        if not user:
            raise Exception("User not found")
        self.repository.remove_user(user, current_user)

    def delete_user(self, user_id: int) -> None:
        user = self.get_user_by_id(user_id)
        if not user:
            raise Exception("User not found")
        self.repository.delete_user(user)
