from sqlalchemy import Column, String
from pydantic import BaseModel, Field
from datetime import datetime

from .entity import Entity
from .enums import RoleEnum, StatusEnum


class User(Entity):
    __tablename__ = "users"

    username = Column(String(50), nullable=False, index=True, unique=True)
    password = Column(String(50), nullable=False, index=True)
    role = Column(String(50), nullable=False, index=True)


class UserDTO(BaseModel):
    username: str
    password: str
    role: RoleEnum = Field(RoleEnum.FINAL_USER, exclude=True, hidden=True)

    class Config:
        schema_extra = {
            "example": {
                "username": "jhon",
                "password": "mypass",
            }
        }


class UserRoleDTO(BaseModel):
    role: RoleEnum

    class Config:
        schema_extra = {
            "example": {
                "role": "owner",
            }
        }


class UserPasswordDTO(BaseModel):
    password: str

    class Config:
        schema_extra = {
            "example": {
                "password": "mypass",
            }
        }


class UserResponse(BaseModel):
    id: int
    username: str
    role: RoleEnum
    status: StatusEnum
    created_at: datetime
    updated_at: datetime
    created_by: str
    updated_by: str

    class Config:
        orm_mode = True
        schema_extra = {
            "example": {
                "id": 1,
                "username": "jhon",
                "role": "final_user",
                "created_at": "2021-08-01T22:00:00",
                "updated_at": "2021-08-01T22:00:00",
                "created_by": "jhon",
                "updated_by": "jhon",
            }
        }

class UserLoginResponse(BaseModel):
    access_token: str
    token_type: str