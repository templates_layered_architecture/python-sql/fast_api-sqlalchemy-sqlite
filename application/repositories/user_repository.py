from datetime import datetime
from typing import List
from sqlalchemy.orm import Session

from ..models import UserDTO, UserRoleDTO, UserPasswordDTO, User, StatusEnum


class UserRepository:
    def __init__(self, db: Session) -> None:
        self.db = db

    def create_user(self, user_data: UserDTO) -> User:
        user = User(
            username=user_data.username,
            password=user_data.password,
            role=user_data.role,
            created_at=datetime.now(),
            updated_at=datetime.now(),
            created_by=user_data.username,
            updated_by=user_data.username,
        )
        self.db.add(user)
        self.db.commit()
        self.db.refresh(user)
        return user

    def get_all_users(self) -> List[User]:
        return self.db.query(User).all()

    def get_active_users(self) -> List[User]:
        return self.db.query(User).filter(User.status == StatusEnum.ACTIVE).all()

    def get_user_by_id(self, id: int) -> User:
        return self.db.query(User).filter(User.id == id).first()

    def get_user_by_username(self, username: str) -> User:
        return self.db.query(User).filter(User.username == username).first()

    def update_user_password(self, user: User, user_password: UserPasswordDTO, current_user: User) -> None:
        user.password = user_password.password
        user.updated_at = datetime.now()
        user.updated_by = current_user.username
        self.db.commit()
        self.db.refresh(user)

    def update_user_role(self, user: User, user_role: UserRoleDTO, current_user: User) -> None:
        user.role = user_role.role
        user.updated_at = datetime.now()
        user.updated_by = current_user.username
        self.db.commit()
        self.db.refresh(user)

    def activate_user(self, user: User, current_user: User) -> None:
        user.status = StatusEnum.ACTIVE
        user.updated_at = datetime.now()
        user.updated_by = current_user.username
        self.db.commit()
        self.db.refresh(user)

    def remove_user(self, user: User, current_user: User) -> None:
        user.status = StatusEnum.INACTIVE
        user.updated_at = datetime.now()
        user.updated_by = current_user.username
        self.db.commit()
        self.db.refresh(user)

    def delete_user(self, user: User) -> None:
        self.db.delete(user)
        self.db.commit()
