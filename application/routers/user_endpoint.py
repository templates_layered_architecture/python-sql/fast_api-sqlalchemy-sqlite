from fastapi import HTTPException, Depends, APIRouter
from fastapi.security import OAuth2PasswordBearer, OAuth2PasswordRequestForm
from jose import jwt, JWTError
from typing import Dict, List

from ..models import (
    User,
    UserDTO,
    UserRoleDTO,
    UserPasswordDTO,
    UserResponse,
    RoleEnum,
    StatusEnum,
    DictEndpointResponse,
    UserLoginResponse,
)
from ..repositories import UserRepository
from ..services import UserService

from ..core import SessionLocal, Settings
from ..common import PasswordManager
from ..common.utils import create_access_token

user_repository = UserRepository(SessionLocal())
user_service = UserService(user_repository)
password_manager = PasswordManager()
settings = Settings()
oauth2_scheme = OAuth2PasswordBearer(tokenUrl="login")

user_router = APIRouter()


#  ------- FUNCTIONS -------


async def get_user_from_db(username: str) -> User:
    user = user_service.get_user_by_username(username)
    if not user:
        raise HTTPException(status_code=400, detail="invalid credentials")
    elif user.status == StatusEnum.INACTIVE:
        raise HTTPException(status_code=400, detail="invalid credentials")
    return user


async def verify_token(token: str) -> User:
    try:
        payload = jwt.decode(token, settings.SECRET_KEY, algorithms=[settings.ALGORITHM])
        username = payload.get("sub")
        user = await get_user_from_db(username)
        return user
    except JWTError:
        raise HTTPException(status_code=401, detail="Invalid Token")


async def get_current_user(token: str = Depends(oauth2_scheme)) -> User:
    user_token = await verify_token(token)
    return user_token


def authorized_roles(user: User) -> bool:
    if user.role in [RoleEnum.ADMIN, RoleEnum.OWNER]:
        return True
    else:
        raise HTTPException(status_code=401, detail="Unauthorized")


#  ------- LOGIN -------


@user_router.post("/login", tags=["Users"], response_model=UserLoginResponse)
async def login(form_data: OAuth2PasswordRequestForm = Depends()) -> Dict[str, str]:
    user = await get_user_from_db(form_data.username)
    if not password_manager.verify_password(form_data.password, user.password):
        raise HTTPException(status_code=400, detail="invalid credentials")
    access_token = create_access_token(username=user.username)
    response = UserLoginResponse(access_token=access_token, token_type="bearer")
    return response


#  ------- ENDPOINTS USER CRUD -------


@user_router.post("/register", tags=["Users"], response_model=UserResponse)
async def create_user(user_data: UserDTO) -> User:
    if user_data.username == settings.ADMIN and user_data.password == settings.SECRET_KEY:
        user_data.role = RoleEnum.ADMIN
    else:
        user_data.role = RoleEnum.FINAL_USER
    hashed_password = password_manager.encrypt_password(user_data.password)
    user_data.password = hashed_password
    user = user_service.create_user(user_data)
    return user


@user_router.get("/users", tags=["Users"])
async def read_all_users(current_user: User = Depends(get_current_user)) -> List[UserResponse]:
    return user_service.get_all_users()


@user_router.get("/users/active", tags=["Users"])
async def read_active_users(current_user: User = Depends(get_current_user)) -> List[UserResponse]:
    return user_service.get_active_users()


@user_router.get("/users/{user_id}", tags=["Users"])
async def read_user(user_id: int, current_user: User = Depends(get_current_user)) -> UserResponse:
    user = user_service.get_user_by_id(user_id)
    if not user:
        raise HTTPException(status_code=404, detail="User not found")
    return user


@user_router.put("/users/password/{user_id}", tags=["Users"], response_model=DictEndpointResponse)
async def update_user_password(
    user_id: int, user_password: UserPasswordDTO, current_user: User = Depends(get_current_user)
) -> Dict[str, str]:
    try:
        hashed_password = password_manager.encrypt_password(user_password.password)
        user_password.password = hashed_password
        user_service.update_user_password(user_id, user_password, current_user)
    except Exception as e:
        raise HTTPException(status_code=404, detail=str(e))
    response = DictEndpointResponse(detail="User password updated successfully")
    return response


@user_router.put("/users/role/{user_id}", tags=["Users"], response_model=DictEndpointResponse)
async def update_user_role(
    user_id: int, user_role: UserRoleDTO, current_user: User = Depends(get_current_user)
) -> Dict[str, str]:
    if authorized_roles(current_user):
        try:
            user_service.update_user_role(user_id, user_role, current_user)
        except Exception as e:
            raise HTTPException(status_code=404, detail=str(e))
        response = DictEndpointResponse(detail="User role updated successfully")
        return response


@user_router.patch("/users/activate/{user_id}", tags=["Users"], response_model=DictEndpointResponse)
async def activate_user(user_id: int, current_user: User = Depends(get_current_user)) -> Dict[str, str]:
    if authorized_roles(current_user):
        try:
            user_service.activate_user(user_id, current_user)
        except Exception as e:
            raise HTTPException(status_code=404, detail=str(e))
        response = DictEndpointResponse(detail="User activate successfully")
        return response


@user_router.patch("/users/remove/{user_id}", tags=["Users"], response_model=DictEndpointResponse)
async def remove_user(user_id: int, current_user: User = Depends(get_current_user)) -> Dict[str, str]:
    if authorized_roles(current_user):
        try:
            user_service.remove_user(user_id, current_user)
        except Exception as e:
            raise HTTPException(status_code=404, detail=str(e))
        response = DictEndpointResponse(detail="User remove successfully")
        return response


@user_router.delete("/users/{user_id}", tags=["Users"], response_model=DictEndpointResponse)
async def delete_user(user_id: int, current_user: User = Depends(get_current_user)) -> Dict[str, str]:
    if authorized_roles(current_user):
        try:
            user_service.delete_user(user_id)
        except Exception as e:
            raise HTTPException(status_code=404, detail=str(e))
        response = DictEndpointResponse(detail="User deleted successfully")
        return response
