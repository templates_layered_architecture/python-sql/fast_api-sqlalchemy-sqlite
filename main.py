import uvicorn
import os
from dotenv import load_dotenv

from fastapi import FastAPI
from fastapi.openapi.utils import get_openapi

from application.models import Base
from application.core.config import engine
from application.routers import user_router

load_dotenv()

app = FastAPI(debug=bool(os.getenv("DEBUG")))
# app.include_router(user_router, prefix="/api/v1")
app.include_router(user_router)
Base.metadata.create_all(engine)


def custom_openapi():
    openapi_schema = get_openapi(
        title="API python layered architecture",
        version="1.0.0",
        description="This is a template API to create projects with layered architecture",
        routes=app.routes,
    )
    app.openapi_schema = openapi_schema
    return app.openapi_schema


app.openapi = custom_openapi

if __name__ == "__main__":
    uvicorn.run(
        "main:app",
        host=os.getenv("HOST"),
        port=int(os.getenv("PORT")),
        reload=os.getenv("RELOAD"),
        log_level=os.getenv("LOG_LEVEL"),
    )


# TODO meter este archivo en application y ver la forma de que funcione y asi poder hacer test al endopint
