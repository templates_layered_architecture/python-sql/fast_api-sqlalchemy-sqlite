import logging


def pytest_runtest_setup():
    logging.getLogger("sqlalchemy.engine").setLevel(logging.CRITICAL)
