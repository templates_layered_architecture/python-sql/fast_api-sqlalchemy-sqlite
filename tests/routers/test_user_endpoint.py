import pytest
from datetime import timedelta
from fastapi import FastAPI, HTTPException
from fastapi.testclient import TestClient

from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker

from ...application.models import Base, User, RoleEnum, StatusEnum
from ...application.repositories import UserRepository
from ...application.services import UserService
from ...application.routers.user_endpoint import (
    user_router,
    get_user_from_db,
    verify_token,
    get_current_user,
    authorized_roles,
)
from ...application.common.utils import create_access_token
from ...application.common import PasswordManager

from ...application.core.settings import Settings

password_manager = PasswordManager()
settings = Settings()

TEST_DATABASE_URL = "sqlite:///:memory:"

engine = create_engine(TEST_DATABASE_URL, echo=True)
Session = sessionmaker(bind=engine)
session = Session()
Base.metadata.create_all(bind=engine)

user_repository = UserRepository(session)
user_service = UserService(user_repository)


@pytest.fixture(scope="module")
def app():
    app = FastAPI()
    app.include_router(user_router)
    yield app


@pytest.fixture(scope="module")
def client(app):
    with TestClient(app) as client:
        yield client


@pytest.mark.asyncio
async def test_get_user_admin_from_db(monkeypatch):
    def mock_get_user_by_username(self, username):
        return User(username="test_user", password="test_password", status=StatusEnum.ACTIVE, role=RoleEnum.ADMIN)

    monkeypatch.setattr(UserService, "get_user_by_username", mock_get_user_by_username)
    result = await get_user_from_db("test_user")

    assert result.username == "test_user"
    assert result.status == StatusEnum.ACTIVE
    assert result.role == RoleEnum.ADMIN

    return result


@pytest.mark.asyncio
async def test_get_user_final_user_from_db(monkeypatch):
    def mock_get_user_by_username(self, username):
        return User(username="test_user", password="test_password", status=StatusEnum.ACTIVE, role=RoleEnum.FINAL_USER)

    monkeypatch.setattr(UserService, "get_user_by_username", mock_get_user_by_username)
    result = await get_user_from_db("test_user")

    assert result.username == "test_user"
    assert result.status == StatusEnum.ACTIVE
    assert result.role == RoleEnum.FINAL_USER

    return result


@pytest.mark.asyncio
async def test_get_user_owner_from_db(monkeypatch):
    def mock_get_user_by_username(self, username):
        return User(username="test_user", password="test_password", status=StatusEnum.ACTIVE, role=RoleEnum.OWNER)

    monkeypatch.setattr(UserService, "get_user_by_username", mock_get_user_by_username)
    result = await get_user_from_db("test_user")

    assert result.username == "test_user"
    assert result.status == StatusEnum.ACTIVE
    assert result.role == RoleEnum.OWNER

    return result


@pytest.mark.asyncio
async def test_get_user_from_db_user_not_found(monkeypatch):
    def mock_get_user_by_username(self, username):
        username = False
        return username

    monkeypatch.setattr(UserService, "get_user_by_username", mock_get_user_by_username)

    with pytest.raises(Exception):
        result = await get_user_from_db("test_user")


@pytest.mark.asyncio
async def test_get_user_from_db_user_inactive(monkeypatch):
    def mock_get_user_by_username(self, username):
        return User(username="test_user", status=StatusEnum.INACTIVE)

    monkeypatch.setattr(UserService, "get_user_by_username", mock_get_user_by_username)

    with pytest.raises(Exception):
        result = await get_user_from_db("test_user")


@pytest.mark.asyncio
async def test_verify_token_admin_success(monkeypatch):
    token = create_access_token("test_user")

    await test_get_user_admin_from_db(monkeypatch)
    result = await verify_token(token)

    assert result.username == "test_user"
    assert result.status == StatusEnum.ACTIVE


@pytest.mark.asyncio
async def test_verify_token_final_user_success(monkeypatch):
    token = create_access_token("test_user")

    await test_get_user_final_user_from_db(monkeypatch)
    result = await verify_token(token)

    assert result.username == "test_user"
    assert result.status == StatusEnum.ACTIVE


@pytest.mark.asyncio
async def test_verify_token_invalid_token():
    token = "your_invalid_token"

    with pytest.raises(HTTPException) as exc_info:
        await verify_token(token)

    assert exc_info.value.status_code == 401
    assert exc_info.value.detail == "Invalid Token"


@pytest.mark.asyncio
async def test_get_current_admin_user(monkeypatch):
    token = create_access_token("test_user")

    await test_verify_token_admin_success(monkeypatch)

    result = await get_current_user(token)

    assert result.username == "test_user"
    assert result.status == StatusEnum.ACTIVE

    return result


@pytest.mark.asyncio
async def test_get_current_final_user(monkeypatch):
    token = create_access_token("test_user")

    await test_verify_token_final_user_success(monkeypatch)

    result = await get_current_user(token)

    assert result.username == "test_user"
    assert result.status == StatusEnum.ACTIVE

    return result


def test_authorized_roles():
    test_user = User(username="test_user", role=RoleEnum.ADMIN)

    result = authorized_roles(test_user)

    assert result == True


def test_authorized_roles_not_authorized():
    test_user = User(username="test_user", role=RoleEnum.FINAL_USER)

    with pytest.raises(Exception):
        authorized_roles(test_user)


#  ------- TEST LOGIN -------


@pytest.mark.asyncio
async def test_login(monkeypatch, client):
    await test_get_user_admin_from_db(monkeypatch)

    def mock_verify_password(self, form_password, user_password):
        form_password = "test_password"
        user_password = "test_password"
        return form_password == user_password

    monkeypatch.setattr(PasswordManager, "verify_password", mock_verify_password)

    login_data = {"username": "test_user", "password": "test_password"}
    response = client.post("/login", data=login_data)
    assert response.status_code == 200
    assert "access_token" in response.json()
    assert response.json()["token_type"] == "bearer"


@pytest.mark.asyncio
async def test_login_wrong_password(monkeypatch, client):
    await test_get_user_admin_from_db(monkeypatch)

    def mock_verify_password(self, form_password, user_password):
        return False

    monkeypatch.setattr(PasswordManager, "verify_password", mock_verify_password)

    login_data = {"username": "test_user", "password": "test_password"}
    response = client.post("/login", data=login_data)

    expected_response = {"detail": "invalid credentials"}

    assert response.status_code == 400
    assert response.json() == expected_response


# ------- TEST USER CRUD -------


def test_create_user_admin(monkeypatch, client):
    def mock_create_user(self, user):
        test_user_data = {
            "id": 1,
            "username": settings.ADMIN,
            "password": settings.SECRET_KEY,
            "role": "admin",
            "status": "active",
            "created_at": "2023-05-12 06:47:22.425361",
            "updated_at": "2023-05-12 06:47:22.425361",
            "created_by": "test_user_register",
            "updated_by": "test_user_register",
        }
        return test_user_data

    monkeypatch.setattr(UserService, "create_user", mock_create_user)

    test_user_data = {"username": settings.ADMIN, "password": settings.SECRET_KEY}

    response = client.post("/register", json=test_user_data)
    assert response.status_code == 200
    assert response.json()["username"] == test_user_data["username"]
    assert response.json()["role"] == RoleEnum.ADMIN


def test_create_final_user(monkeypatch, client):
    def mock_create_user(self, user):
        test_user_data = {
            "id": 1,
            "username": "test_user_register",
            "password": "secret",
            "role": "final_user",
            "status": "active",
            "created_at": "2023-05-12 06:47:22.425361",
            "updated_at": "2023-05-12 06:47:22.425361",
            "created_by": "test_user_register",
            "updated_by": "test_user_register",
        }
        return test_user_data

    monkeypatch.setattr(UserService, "create_user", mock_create_user)

    hash_password = password_manager.encrypt_password("secret")
    test_user_data = {"username": "test_user_register", "password": hash_password}

    response = client.post("/register", json=test_user_data)
    assert response.status_code == 200
    assert response.json()["username"] == test_user_data["username"]
    assert response.json()["role"] == RoleEnum.FINAL_USER
    assert password_manager.verify_password("secret", hash_password) == True


@pytest.mark.asyncio
async def test_read_all_users(monkeypatch, client):
    result = await test_get_current_admin_user(monkeypatch)
    token = create_access_token(result.username)

    def mock_get_all_users(self):
        user_list = [
            User(
                id="1",
                username="test_user",
                password="test_password",
                role="final_user",
                status="active",
                created_at="2023-05-12 06:47:22.425361",
                updated_at="2023-05-12 06:47:22.425361",
                created_by="test_user",
                updated_by="test_user",
            )
        ]
        return user_list

    monkeypatch.setattr(UserService, "get_all_users", mock_get_all_users)

    response = client.get("/users", headers={"Authorization": f"Bearer {token}"})

    expected_response = [
        {
            "id": 1,
            "username": "test_user",
            "role": "final_user",
            "status": "active",
            "created_at": "2023-05-12T06:47:22.425361",
            "updated_at": "2023-05-12T06:47:22.425361",
            "created_by": "test_user",
            "updated_by": "test_user",
        }
    ]

    assert response.status_code == 200
    assert response.json() == expected_response


@pytest.mark.asyncio
async def test_read_active_users(monkeypatch, client):
    result = await test_get_current_admin_user(monkeypatch)
    token = create_access_token(result.username)

    def mock_get_active_users(self):
        user_response_list = [
            User(
                id="1",
                username="test_user",
                password="test_password",
                role="final_user",
                status="active",
                created_at="2023-05-12 06:47:22.425361",
                updated_at="2023-05-12 06:47:22.425361",
                created_by="test_user",
                updated_by="test_user",
            )
        ]
        return user_response_list

    monkeypatch.setattr(UserService, "get_active_users", mock_get_active_users)

    response = client.get("/users/active", headers={"Authorization": f"Bearer {token}"})

    expected_response = [
        {
            "id": 1,
            "username": "test_user",
            "role": "final_user",
            "status": "active",
            "created_at": "2023-05-12T06:47:22.425361",
            "updated_at": "2023-05-12T06:47:22.425361",
            "created_by": "test_user",
            "updated_by": "test_user",
        }
    ]

    assert response.status_code == 200
    assert response.json() == expected_response


@pytest.mark.asyncio
async def test_read_user(monkeypatch, client):
    result = await test_get_current_admin_user(monkeypatch)
    token = create_access_token(result.username)

    def mock_get_user_by_id(self, user_id):
        user_response = User(
            id="1",
            username="test_user",
            password="test_password",
            role="final_user",
            status="active",
            created_at="2023-05-12 06:47:22.425361",
            updated_at="2023-05-12 06:47:22.425361",
            created_by="test_user",
            updated_by="test_user",
        )

        return user_response

    monkeypatch.setattr(UserService, "get_user_by_id", mock_get_user_by_id)

    response = client.get("/users/1", headers={"Authorization": f"Bearer {token}"})

    expected_response = {
        "id": 1,
        "username": "test_user",
        "role": "final_user",
        "status": "active",
        "created_at": "2023-05-12T06:47:22.425361",
        "updated_at": "2023-05-12T06:47:22.425361",
        "created_by": "test_user",
        "updated_by": "test_user",
    }

    assert response.status_code == 200
    assert response.json() == expected_response


@pytest.mark.asyncio
async def test_read_user_not_found(monkeypatch, client):
    result = await test_get_current_admin_user(monkeypatch)
    token = create_access_token(result.username)

    def mock_get_user_by_id(self, user_id):
        return None

    monkeypatch.setattr(UserService, "get_user_by_id", mock_get_user_by_id)

    response = client.get("/users/1", headers={"Authorization": f"Bearer {token}"})

    expected_response = {"detail": "User not found"}

    assert response.status_code == 404
    assert response.json() == expected_response


@pytest.mark.asyncio
async def test_update_user_password(monkeypatch, client):
    result = await test_get_current_admin_user(monkeypatch)
    token = create_access_token(result.username)

    def mock_update_user_password(self, user_id, user_password, current_user):
        user_response = User(
            id="1",
            username="test_user",
            password="test_password",
            role="final_user",
            status="active",
            created_at="2023-05-12 06:47:22.425361",
            updated_at="2023-05-12 06:47:22.425361",
            created_by="test_user",
            updated_by="test_user",
        )

        return user_response

    monkeypatch.setattr(UserService, "update_user_password", mock_update_user_password)

    update_data = {"password": "test_update_password"}

    response = client.put("/users/password/1", json=update_data, headers={"Authorization": f"Bearer {token}"})

    expected_response = {"detail": "User password updated successfully"}

    assert response.status_code == 200
    assert response.json() == expected_response


@pytest.mark.asyncio
async def test_update_user_password_user_not_found(monkeypatch, client):
    result = await test_get_current_admin_user(monkeypatch)
    token = create_access_token(result.username)

    def mock_update_user_password(self, user_id, user_password, current_user):
        raise Exception("User not found")

    monkeypatch.setattr(UserService, "update_user_password", mock_update_user_password)

    update_data = {"password": "test_update_password"}

    response = client.put("/users/password/1", json=update_data, headers={"Authorization": f"Bearer {token}"})

    expected_response = {"detail": "User not found"}

    assert response.status_code == 404
    assert response.json() == expected_response


@pytest.mark.asyncio
async def test_update_user_role(monkeypatch, client):
    result = await test_get_current_admin_user(monkeypatch)
    token = create_access_token(result.username)

    def mock_update_user_role(self, user_id, user_password, current_user):
        user_response = User(
            id="1",
            username="test_user",
            password="test_password",
            role="final_user",
            status="active",
            created_at="2023-05-12 06:47:22.425361",
            updated_at="2023-05-12 06:47:22.425361",
            created_by="test_user",
            updated_by="test_user",
        )

        return user_response

    monkeypatch.setattr(UserService, "update_user_role", mock_update_user_role)

    update_data = {"role": "final_user"}

    response = client.put("/users/role/1", json=update_data, headers={"Authorization": f"Bearer {token}"})

    expected_response = {"detail": "User role updated successfully"}

    assert response.status_code == 200
    assert response.json() == expected_response


@pytest.mark.asyncio
async def test_update_user_role_wrong_role(monkeypatch, client):
    result = await test_get_current_admin_user(monkeypatch)
    token = create_access_token(result.username)

    update_data = {"role": "wrong_role"}

    response = client.put("/users/role/1", json=update_data, headers={"Authorization": f"Bearer {token}"})

    expected_response = {
        "detail": [
            {
                "loc": ["body", "role"],
                "msg": (
                    "value is not a valid enumeration member; permitted: 'final_user', 'admin', 'owner', 'guest',"
                    " 'moderator'"
                ),
                "type": "type_error.enum",
                "ctx": {"enum_values": ["final_user", "admin", "owner", "guest", "moderator"]},
            }
        ]
    }

    assert response.status_code == 422
    assert response.json() == expected_response


@pytest.mark.asyncio
async def test_update_user_role_user_not_found(monkeypatch, client):
    result = await test_get_current_admin_user(monkeypatch)
    token = create_access_token(result.username)

    def mock_update_user_role(self, user_id, user_password, current_user):
        raise Exception("User not found")

    monkeypatch.setattr(UserService, "update_user_role", mock_update_user_role)

    update_data = {"role": "final_user"}

    response = client.put("/users/role/1", json=update_data, headers={"Authorization": f"Bearer {token}"})

    expected_response = {"detail": "User not found"}

    assert response.status_code == 404
    assert response.json() == expected_response


@pytest.mark.asyncio
async def test_update_user_role_user_unauthorized(monkeypatch, client):
    result = await test_get_current_final_user(monkeypatch)
    token = create_access_token(result.username)

    update_data = {"role": "final_user"}

    response = client.put("/users/role/1", json=update_data, headers={"Authorization": f"Bearer {token}"})

    expected_response = {"detail": "Unauthorized"}

    assert response.status_code == 401
    assert response.json() == expected_response


@pytest.mark.asyncio
async def test_activate_user(monkeypatch, client):
    result = await test_get_current_admin_user(monkeypatch)
    token = create_access_token(result.username)

    def mock_activate_user(self, user_id, current_user):
        return None

    monkeypatch.setattr(UserService, "activate_user", mock_activate_user)

    response = client.patch("/users/activate/1", headers={"Authorization": f"Bearer {token}"})

    expected_response = {"detail": "User activate successfully"}

    assert response.status_code == 200
    assert response.json() == expected_response


@pytest.mark.asyncio
async def test_activate_user_not_found(monkeypatch, client):
    result = await test_get_current_admin_user(monkeypatch)
    token = create_access_token(result.username)

    def mock_activate_user(self, user_id, current_user):
        raise Exception("User not found")

    monkeypatch.setattr(UserService, "activate_user", mock_activate_user)

    response = client.patch("/users/activate/1", headers={"Authorization": f"Bearer {token}"})

    expected_response = {"detail": "User not found"}

    assert response.status_code == 404
    assert response.json() == expected_response


@pytest.mark.asyncio
async def test_remove_user(monkeypatch, client):
    result = await test_get_current_admin_user(monkeypatch)
    token = create_access_token(result.username)

    def mock_remove_user(self, user_id, current_user):
        return None

    monkeypatch.setattr(UserService, "remove_user", mock_remove_user)

    response = client.patch("/users/remove/1", headers={"Authorization": f"Bearer {token}"})

    expected_response = {"detail": "User remove successfully"}

    assert response.status_code == 200
    assert response.json() == expected_response


@pytest.mark.asyncio
async def test_remove_user_not_found(monkeypatch, client):
    result = await test_get_current_admin_user(monkeypatch)
    token = create_access_token(result.username)

    def mock_remove_user(self, user_id, current_user):
        raise Exception("User not found")

    monkeypatch.setattr(UserService, "remove_user", mock_remove_user)

    response = client.patch("/users/remove/1", headers={"Authorization": f"Bearer {token}"})

    expected_response = {"detail": "User not found"}

    assert response.status_code == 404
    assert response.json() == expected_response


@pytest.mark.asyncio
async def test_delete_user(monkeypatch, client):
    result = await test_get_current_admin_user(monkeypatch)
    token = create_access_token(result.username)

    def mock_delete_user(self, user_id):
        return None

    monkeypatch.setattr(UserService, "delete_user", mock_delete_user)

    response = client.delete("/users/1", headers={"Authorization": f"Bearer {token}"})

    expected_response = {"detail": "User deleted successfully"}

    assert response.status_code == 200
    assert response.json() == expected_response


@pytest.mark.asyncio
async def test_delete_user_not_found(monkeypatch, client):
    result = await test_get_current_admin_user(monkeypatch)
    token = create_access_token(result.username, timedelta(minutes=50))

    def mock_delete_user(self, user_id):
        raise Exception("User not found")

    monkeypatch.setattr(UserService, "delete_user", mock_delete_user)

    response = client.delete("/users/1", headers={"Authorization": f"Bearer {token}"})

    expected_response = {"detail": "User not found"}

    assert response.status_code == 404
    assert response.json() == expected_response
