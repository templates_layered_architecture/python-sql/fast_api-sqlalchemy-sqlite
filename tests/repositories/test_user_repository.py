from datetime import datetime
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
from typing import List

from ...application.models import Base, User, UserDTO, UserPasswordDTO, UserRoleDTO, RoleEnum, StatusEnum
from ...application.repositories import UserRepository

# Configuración de la base de datos de prueba en memoria
TEST_DATABASE_URL = "sqlite:///:memory:"

engine = create_engine(TEST_DATABASE_URL, echo=True)
Session = sessionmaker(bind=engine)
session = Session()
Base.metadata.create_all(bind=engine)

user_repository = UserRepository(session)


def test_create_user():
    user_data = UserDTO(username="test_user", password="test_password", role="admin")
    created_user = user_repository.create_user(user_data)

    assert isinstance(created_user, User)
    assert created_user.username == "test_user"
    assert created_user.password == "test_password"
    assert created_user.role == "admin"
    assert isinstance(created_user.created_at, datetime)
    assert isinstance(created_user.updated_at, datetime)
    assert created_user.created_by == "test_user"
    assert created_user.updated_by == "test_user"

    session.close()


def test_get_all_users():
    users = user_repository.get_all_users()
    test_user = users[0]

    assert isinstance(users, List)
    assert isinstance(test_user, User)
    assert test_user.username == "test_user"

    session.close()


def test_get_active_users():
    users = user_repository.get_active_users()
    test_user = users[0]

    assert isinstance(users, List)
    assert isinstance(test_user, User)
    assert test_user.username == "test_user"

    session.close()


def test_get_active_users_ignore_inactive():
    users = user_repository.get_active_users()
    assert len(users) == 1

    test_user = users[0]
    user_repository.remove_user(test_user, test_user)

    users = user_repository.get_active_users()
    assert len(users) == 0

    session.close()


def test_get_user_by_id():
    test_user = user_repository.get_user_by_id(1)
    assert isinstance(test_user, User)
    assert test_user.username == "test_user"

    session.close()


def test_get_user_by_username():
    test_user = user_repository.get_user_by_username("test_user")
    assert isinstance(test_user, User)
    assert test_user.username == "test_user"

    session.close()


def test_update_user_password():
    test_user = user_repository.get_user_by_id(1)
    previous_password = test_user.password
    test_user_password = UserPasswordDTO(password="test_user_password")
    user_repository.update_user_password(test_user, test_user_password, test_user)
    test_user_updated = user_repository.get_user_by_id(1)

    assert previous_password != test_user_updated.password

    session.close()


def test_update_user_role():
    test_user = user_repository.get_user_by_id(1)
    previous_role = test_user.role
    test_user_role = UserRoleDTO(role=RoleEnum.OWNER)
    user_repository.update_user_role(test_user, test_user_role, test_user)
    test_user_updated = user_repository.get_user_by_id(1)

    assert previous_role != test_user_updated.role

    session.close()


def test_activate_user():
    test_user = user_repository.get_user_by_id(1)
    previous_status = test_user.status
    user_repository.activate_user(test_user, test_user)

    assert previous_status != test_user.status
    assert test_user.status == StatusEnum.ACTIVE.value

    session.close()


def test_remove_user():
    test_user = user_repository.get_user_by_id(1)
    previous_status = test_user.status
    user_repository.remove_user(test_user, test_user)

    assert previous_status != test_user.status
    assert test_user.status == StatusEnum.INACTIVE.value

    session.close()


def test_delete_user():
    user_data_delete = UserDTO(username="test_user_delete", password="test_password", role="final_user")
    test_user_delete = user_repository.create_user(user_data_delete)

    assert len(user_repository.get_all_users()) == 2

    user_repository.delete_user(test_user_delete)

    assert len(user_repository.get_all_users()) == 1

    session.close()
