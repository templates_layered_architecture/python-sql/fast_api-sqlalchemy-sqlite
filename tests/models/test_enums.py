from ...application.models import RoleEnum, StatusEnum


def test_role_enum():
    assert RoleEnum.FINAL_USER.value == "final_user"
    assert RoleEnum.ADMIN.value == "admin"
    assert RoleEnum.OWNER.value == "owner"
    assert RoleEnum.GUEST.value == "guest"
    assert RoleEnum.MODERATOR.value == "moderator"


def test_status_enum():
    assert StatusEnum.ACTIVE.value == "active"
    assert StatusEnum.INACTIVE.value == "inactive"
