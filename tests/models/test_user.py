import pytest
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
from datetime import datetime

from ...application.models import Entity, User

# Configuración de la base de datos de prueba en memoria
TEST_DATABASE_URL = "sqlite:///:memory:"


# Fixture de prueba para la sesión de la base de datos
@pytest.fixture(scope="module")
def db_session():
    # Crea la conexión a la base de datos de prueba
    engine = create_engine(TEST_DATABASE_URL)
    Session = sessionmaker(bind=engine)
    session = Session()

    # Crea las tablas en la base de datos de prueba
    Entity.metadata.create_all(engine)

    # Retorna la sesión de la base de datos
    yield session

    # Cierra la sesión y elimina las tablas después de las pruebas
    session.close()
    Entity.metadata.drop_all(engine)


# Test de ejemplo para la clase User
def test_user(db_session):
    # Crea un nuevo usuario
    user = User(
        username="john",
        password="password123",
        role="admin",
        created_at=datetime.now(),
        updated_at=datetime.now(),
        created_by="jhon",
        updated_by="john",
    )
    db_session.add(user)
    db_session.commit()

    # Consulta el usuario de la base de datos
    queried_user = db_session.query(User).filter_by(username="john").first()

    # Verifica que el usuario existe en la base de datos
    assert queried_user is not None
    assert queried_user.username == "john"
    assert queried_user.password == "password123"
    assert queried_user.role == "admin"
